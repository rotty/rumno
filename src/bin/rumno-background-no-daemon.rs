// This module compiles into the binary to start the notification daemon. It receives the
// notification updates through a dbus interface

use rumno::dbus_util;
use rumno::ui::gtk_channel_container::GtkChannelContainer;
use rumno::ui::gtk_main;
use rumno::common::logging::setup_logger;

use glib;

use std::thread;

fn main() {
    setup_logger();
    setup_and_run_main_loop();
}

fn setup_and_run_main_loop() {
    let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    thread::spawn(move || {
        let sender = GtkChannelContainer { sender };
        let result = dbus_util::server::setup_dbus_server(sender);

        match result {
            Ok(_reply) => println!("Daemon finished its endless loop for some reason (that's probably bad)"),
            Err(err) => eprintln!("Error: {} \nPlease check if the daemon is running", err),
        };
    });

    gtk_main::run_gtk(receiver);
}
