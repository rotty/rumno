pub mod gtk_channel_container;
pub mod gtk_main;

mod app;
mod drawing;
mod message;

extern crate cairo;
extern crate resvg;
