#! /bin/sh

# Build without root privilig, in order to use the user rust toolchain
echo "> Build binaries"
cargo build --release --manifest-path code/Cargo.toml

echo "> Copie binaries into system path (/usr/local/bin), root privileges required"
sudo cp code/target/release/rumno /usr/local/bin
sudo cp code/target/release/rumno-background /usr/local/bin

echo "> Copie ressources into system path (/usr/local/share/rumno)"
sudo mkdir -p /usr/local/share/rumno
sudo cp -a res /usr/local/share/rumno



